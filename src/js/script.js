$(document).ready(function () {
    $("#video-play").click(function () {
        $("#video-play").css("display", "none");
        $("#video-block").css("display", "block").attr("autoplay", "true");
        $(".section-video").css({
            background: "none",
            opacity: "1"
        });
    });

    $("#nav-search").click(function () {
        $(this).toggleClass("glyphicon-remove");
        $(".menu-item").toggleClass("hidden");
    });

    $(".form-control").focus(function () {
        $(this).parent(".input-group").css({
            "box-shadow": "0px .4px 0px 1.3px rgb(109, 89, 180)"
        });
    }).focusout(function () {
        $(this).parent(".input-group").css({
            "box-shadow": "none"
        })
    });

    $("#scrollTop").click(function () {
        //scrollTo(0,0);
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

});